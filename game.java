// Interface Componente (Ataque)
interface Attack {
    void performAttack();
}

// Implementação Concreta do Componente (Ataque Básico)
class BasicAttack implements Attack {
    @Override
    public void performAttack() {
        System.out.println("Ataque Básico!");
    }
}

// Decorador abstrato
abstract class AttackDecorator implements Attack {
    protected Attack decoratedAttack;

    public AttackDecorator(Attack decoratedAttack) {
        this.decoratedAttack = decoratedAttack;
    }

    @Override
    public void performAttack() {
        decoratedAttack.performAttack();
    }
}

// Implementação Concreta do Decorador (Poder Adicional 1)
class PowerUp1 extends AttackDecorator {
    public PowerUp1(Attack decoratedAttack) {
        super(decoratedAttack);
    }

    @Override
    public void performAttack() {
        super.performAttack();
        System.out.println("Poder Adicional 1 Ativado!");
    }
}

// Implementação Concreta do Decorador (Poder Adicional 2)
class PowerUp2 extends AttackDecorator {
    public PowerUp2(Attack decoratedAttack) {
        super(decoratedAttack);
    }

    @Override
    public void performAttack() {
        super.performAttack();
        System.out.println("Poder Adicional 2 Ativado!");
    }
}

// Implementação Concreta do Decorador (Poder Adicional 3)
class PowerUp3 extends AttackDecorator {
    public PowerUp3(Attack decoratedAttack) {
        super(decoratedAttack);
    }

    @Override
    public void performAttack() {
        super.performAttack();
        System.out.println("Poder Adicional 3 Ativado!");
    }
}

// Classe principal com método main() para simular a execução do jogo
public class game {
    public static void main(String[] args) {
        // Ataque básico do jogador
        Attack basicAttack = new BasicAttack();

        // Adicionando poderes dinamicamente em tempo de execução
        Attack enhancedAttack = new PowerUp1(new PowerUp2(new PowerUp3(basicAttack)));

        // Simulando o ataque aprimorado
        System.out.println("Jogador realizando ataque aprimorado:");
        enhancedAttack.performAttack();
    }
}


//Gustavo Augusto Aniceto Tolentino