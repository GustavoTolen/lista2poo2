// Interface Componente 

interface Component { 

    String render(); 

  

    String text(); 

} 

  

// Implementação Concreta do Componente (Palavra simples) 

class SimpleWord implements Component { 

    private String content; 

  

    public SimpleWord(String content) { 

        this.content = content; 

    } 

  

    public String render() { 

        return content; 

    } 

  

    public String text() { 

        return content; 

    } 

} 

  

// Decorador abstrato 

abstract class Decorator implements Component { 

    protected Component component; 

  

    public Decorator(Component component) { 

        this.component = component; 

    } 

  

    public abstract String render(); 

  

    public abstract String text(); 

} 

  

// Implementação Concreta do Decorador (Negrito) 

class BoldDecorator extends Decorator { 

    public BoldDecorator(Component component) { 

        super(component); 

    } 

  

    public String render() { 

        return "<b>" + component.render() + "</b>"; 

    } 

  

    public String text() { 

        return "<b>" + component.text() + "</b>"; 

    } 

} 

  

// Implementação Concreta do Decorador (Itálico) 

class ItalicDecorator extends Decorator { 

    public ItalicDecorator(Component component) { 

        super(component); 

    } 

  

    public String render() { 

        return "<i>" + component.render() + "</i>"; 

    } 

  

    public String text() { 

        return "<i>" + component.text() + "</i>"; 

    } 

} 

  

// Implementação Concreta do Decorador (Sublinhado) 

class UnderlineDecorator extends Decorator { 

    public UnderlineDecorator(Component component) { 

        super(component); 

    } 

  

    public String render() { 

        return "<u>" + component.render() + "</u>"; 

    } 

  

    public String text() { 

        return "<u>" + component.text() + "</u>"; 

    } 

} 

  

// Classe de exemplo para ilustrar o comportamento 

public class TextDecorationExample { 

    public static void main(String[] args) { 

        // Palavra simples 

        Component simpleWord = new SimpleWord("normal"); 

        System.out.println("normal " + simpleWord.render()); 

  

        // Palavra em negrito 

        Component boldWord = new BoldDecorator(simpleWord); 

        System.out.println("negrito " + boldWord.render()); 

  

        // Palavra em itálico 

        Component italicWord = new ItalicDecorator(simpleWord); 

        System.out.println("itálico " + italicWord.render()); 

  

        // Palavra sublinhada 

        Component underlineWord = new UnderlineDecorator(simpleWord); 

        System.out.println("sublinhado " + underlineWord.render()); 

  

        // Palavra em negrito e itálico 

        Component boldItalicWord = new BoldDecorator(new ItalicDecorator(simpleWord)); 

        System.out.println("negrito e itálico " + boldItalicWord.render()); 

    } 

} 